# Luna's new website :3

Luna's new website, since 2025.

## Environment Setup

```text
DATABASE_URL=postgres://USERNAME:PASSWORD@HOST:PORT/DATABASE
```

## License

Please follow the GNU GPL v3 attached to this project.

Content written about Luna :3 is copyrighted by her, the rest of code is free to take as long as it complies with
the
license.

You are free to use this website, but please change the project name and contents of the website.

## Contributing

If you want to waste time with my own personal website, go ahead, fork it and contribute. Even if I use my own Forgejo
instance, I'll be happy to accept any pull requests on GitHub or Codeberg.
