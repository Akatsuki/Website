/*
 *     mldchan's Personal Website
 *     Copyright (C) 2024  エムエルディー
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import React from "react";


import Main from "@/components/Generic/Main";
import {Header} from "@/components/Generic/Header";
import Content from "@/components/Generic/Content";
import Heading from "@/components/Generic/Heading";
import Offset from "@/components/Generic/Offset";
import Metadata from "@/components/Generic/Metadata";
import Footer from "@/components/Generic/Footer";
import {InferGetServerSidePropsType} from "next";
import {getVisits} from "@/backend/visitsCounter";

const PrivacyHeading = (props: { children: string }) => <h2 className={'text-xl font-bold my-4'}>{props.children}</h2>;

export async function getServerSideProps() {
    const visits = await getVisits({route: '/project/akabot/privacy'});

    return {
        props: {
            visits
        }
    }
}

export default function AkabotPrivacy(props: InferGetServerSidePropsType<typeof getServerSideProps>) {
    return (
        <Main>
            <Metadata title={'Akabot Privacy Policy'} description={'Akabot Discord bot privacy policy'}/>
            <Header highlight={'projects'}/>
            <Content>
                <Offset amt={4}/>
                <Heading level={1}>Akabot Privacy Policy</Heading>
                <p>Last updated: 2024/05/16</p>
                <p>
                    The public instance of Akabot Discord bot takes privacy very seriously. It is as private as if you
                    would
                    host it yourself. If you think this privacy statement is way too privacy intrusive, check out the
                    Discord
                    privacy
                    policy.
                </p>
                <PrivacyHeading>What Akabot stores in its databases</PrivacyHeading>
                <p className={'text-lg'}>Each feature of the bot stores as little data as possible. Here&apos;s a quick
                    overview of what is
                    stored and
                    what is associated with every feature:</p>
                <ul className={'list-disc ml-4'}>
                    <li>
                        <strong>Analytics</strong> - There&apos;s a feature in Akabot which counts which command is
                        executed how
                        many times, globally. This feature doesn&apos;t associate any command runs or button clicks with
                        any
                        users or servers,
                        it only counts them. This feature is here to see which features are used the most on the bot and
                        what
                        should be added or improved in the future.
                    </li>
                    <li>
                        <strong>Anti Raid</strong> - The Anti Raid feature stores only your settings, and if you count
                        the RAM
                        of the server as storage, it stores the number of joins and the number of messages and the dates
                        they
                        were last updated
                        for a maximum of the user&apos;s desired time. No user ID&apos;s or messages are stored, only
                        numbers.
                    </li>
                    <li>
                        <strong>Automod Actions</strong> - User defined actions along with all information about them
                        (names,
                        actions and the optional message) are stored. When an automod action with a name gets triggered,
                        the
                        user defined actions
                        stored will be executed.
                    </li>
                    <li><strong>Bot Help</strong> - The bot help commands don&apos;t store any user information.</li>
                    <li><strong>Chat Revive</strong> - The Chat Revive feature only stores which role has been chosen,
                        channels
                        which are active and the last message time sent in that channel.
                    </li>
                    <li>
                        <strong>Chat Streaks</strong> - The Chat Streaks feature stored the date you started your chat
                        streak
                        along with the date of your last sent message. Using these 2 dates, the bot calculates the rest
                        of the
                        messages. You
                        can disable the notifications of this feature using `/user_settings chat_streaks_alerts off` and
                        you can
                        always check your streak by using the `/streak` command.
                    </li>
                    <li>
                        <strong>Chat Summary</strong> - The Chat Summary feature stores the amount of messages sent in a
                        channel
                        daily, along with who has sent how many messages in that channel. Only the top 5 chatters will
                        be shown
                        in the summary.
                        No message content is stored. All numbers in this module are cleared every midnight.
                    </li>
                    <li>
                        <strong>Feedback Commands</strong> - This module does explicitly state before usage that it does
                        share
                        user entered information, along with the user name and user ID with GitHub. No user information
                        is saved
                        with these
                        commands. If the user doesn&apos;t want this to happen, they can use GitHub with their personal
                        accounts. Any of the other commands like `/about` , `/privacy` , `/tos` do not save any user
                        information.
                    </li>
                    <li>
                        <strong>Giveaways</strong> - The giveaways module stores the channel the giveaway was started
                        in, the
                        item entered and the ending date, calculated when the giveaway is created. The creator of the
                        giveaway
                        is not stored.
                    </li>
                    <li>
                        <strong>Leveling</strong> - Each user has an associated number of XP points which is stored and
                        associated to every user. A list of leveling multipliers, the server general leveling
                        multiplier, and
                        the XP required to
                        level up are stored and associated to every server.
                    </li>
                    <li><strong>Logging</strong> - The logging channel is stored and associated with the server. The bot
                        then
                        listens to Discord changes and creates messages from available information in your logs channel.
                    </li>
                    <li>
                        <strong>Moderation</strong> - No information is stored when using kick, ban and timeout
                        commands. When
                        using warnings, a list of warnings created using the bot are stored. Warning actions are also
                        stored and
                        associated
                        with the server and performed after adding a warning to the user.
                    </li>
                    <li><strong>Per User Settings</strong> - All per user settings have settings associated with the
                        user.
                    </li>
                    <li>
                        <strong>Reaction Roles</strong> - You wouldn&apos;t believe it, but this module doesn&apos;t
                        store any
                        information at all. The Role ID&apos;s and types of the reaction roles are stored in the
                        button&apos;s
                        custom ID itself.
                    </li>
                    <li><strong>Roles on Join</strong> - The list of roles to be added on join are stored and associated
                        with
                        the server.
                    </li>
                    <li><strong>Server Settings</strong> - All settings stored are associated with the server.</li>
                    <li><strong>Verification</strong> - The verification difficulty and role are stored per server.</li>
                    <li><strong>Welcome and Goodbye</strong> - All user defined settings are stored and associated with
                        the
                        server.
                    </li>
                </ul>
                <p className={'mt-4 text-lg'}>Backend modules mostly don&apos;t mess with any user data or any server
                    data.</p>
                <ul className={'list-disc ml-4'}>
                    <li><strong>Heartbeat</strong> - Sends periodic network requests to a URL every set interval.</li>
                    <li>
                        <strong>Power Outage Announcements</strong> - Stores the current time every minute, doesn&apos;t
                        associate it with any server or user. Reads the last saved time and current system time on
                        startup of
                        the bot and makes
                        an announcement in a channel preconfigured in the bot configuration if the bot was offline for a
                        longer
                        period of time.
                    </li>
                </ul>
                <p>
                    The developer doesn&apos;t have any access to read user data or any user control over the bot. They
                    always
                    write scripts to update the database and performs it on the test database, which contains one server
                    and 5
                    users,
                    4 of them are all variants of the bot. The only exception is analytics and the number of servers.
                </p>
                <PrivacyHeading>Linked or connected services</PrivacyHeading>
                <p>The services that are connected to the bot are listed below:</p>
                <ul>
                    <li>
                        <strong>GlitchTip</strong> is used to capture errors. Errors do not show me any personal
                        information,
                        only generic messages. These errors aren&apos;t associated with any server or user, only with
                        the line
                        of code which
                        caused the error. The used GlitchTip instance is a self-hosted one, running on the same server
                        as the
                        bot itself.
                    </li>
                </ul>
                <PrivacyHeading>Third parties</PrivacyHeading>
                <p>The Discord bot can share data with GitHub, only if a user executes one of the `/feedback bug` or
                    `/feedback
                    suggest` commands. These commands explicitly state before running that they share this data with
                    GitHub.</p>
                <p>The bot doesn&apos;t share any user data with any other party, except the one listed above.</p>
                <PrivacyHeading>Changes to the privacy statement</PrivacyHeading>
                <p>
                    If the privacy statement ever changes, which it might in the future, a Discord ping in the support
                    server
                    will alert you about it, along with a summary of the changes, so you don&apos;t have to read the
                    entire
                    statement
                    again.
                </p>
                <Offset amt={3}/>
                <Footer visits={props.visits} route={'/project/akabot/privacy'}
                        />
            </Content>
        </Main>
    )
}
